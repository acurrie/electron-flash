# Electron Flash Player
___

I was recently curious if it was possible to setup a full screen flash player using Electron. This repository is a working example of a full-screen Adobe Flash player desktop application built-on Electron and ready to be built for Mac or Windows. The included SWF file (skate.swf) is me skateboarding in 2002.

## Usage

1. Clone this repository
2. Open the Terminal and change to the repository directory (electron-flash-master)
3. Type 'npm start' and press Enter


## Disclaimer

I'm fully aware that Adobe Flash is a dead technology and I'm not necessarily a fan of it. Nevertheless, this turned out to be pretty fun.
